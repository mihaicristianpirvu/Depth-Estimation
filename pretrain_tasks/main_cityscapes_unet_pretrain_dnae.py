from main import ModelUNetDilatedConv, CityScapesReader, ModelUNetDilatedConv, changeDirectory, getCallbacks
from neural_wrappers.callbacks import SaveModels, SaveModelsSelfSupervised
from neural_wrappers.pytorch import SelfSupervisedNetwork, maybeCuda
from neural_wrappers.readers import CorrupterReader
import sys
from Mihlib import plot_image, show_plots
import torch.optim as optim
import torch.nn as nn
import torch as tr
import torch.nn.functional as F

def makeArgs(attrDict):
	class FakeArgs: pass
	args = FakeArgs()
	for key in attrDict:
		setattr(args, key, attrDict[key])
	return args

class DNAEModel(SelfSupervisedNetwork):
	def __init__(self, **kwargs):
		super().__init__(ModelUNetDilatedConv(**kwargs))
		self.DNAE_finalConv = nn.Conv2d(in_channels=kwargs["numFilters"], out_channels=3, kernel_size=(1, 1))

	def forward(self, x):
		x = tr.transpose(tr.transpose(x, 1, 3), 2, 3)
		y_down1 = self.baseModel.downBlock1(x)
		y_down1pool = self.baseModel.pool1(y_down1)
		y_down2 = self.baseModel.downBlock2(y_down1pool)
		y_down2pool = self.baseModel.pool2(y_down2)
		y_down3 = self.baseModel.downBlock3(y_down2pool)
		y_down3pool = self.baseModel.pool3(y_down3)

		y_dilate1 = F.relu(self.baseModel.dilate1(y_down3pool))
		y_dilate2 = F.relu(self.baseModel.dilate2(y_dilate1))
		y_dilate3 = F.relu(self.baseModel.dilate2(y_dilate2))
		y_dilate4 = F.relu(self.baseModel.dilate2(y_dilate3))
		y_dilate5 = F.relu(self.baseModel.dilate2(y_dilate4))
		y_dilate6 = F.relu(self.baseModel.dilate2(y_dilate5))
		y_dilate_concatenate = tr.cat([y_dilate1, y_dilate2, y_dilate3, y_dilate4, y_dilate5, y_dilate6], dim=1)

		y_up3 = self.baseModel.up3(y_down3, y_dilate_concatenate)
		y_up3block = self.baseModel.upBlock3(y_up3)
		y_up2 = self.baseModel.up2(y_down2, y_up3block)
		y_up2block = self.baseModel.upBlock2(y_up2)
		y_up1 = self.baseModel.up1(y_down1, y_up2block)
		y_up1block = self.baseModel.upBlock1(y_up1)

		y_final = self.DNAE_finalConv(y_up1block)
		y_final = tr.transpose(tr.transpose(y_final, 1, 3), 1, 2)
		return y_final

	def pretrainLayersSetup(self):
		if self.pretrain == False:
			self.DNAE_finalConv = None

def lossFn(y, t):
	return ((y - t)**2).sum(dim=1).sum(dim=1).sum(dim=1).mean()

def main():
	cityscapesReader = CityScapesReader(sys.argv[1], imageShape=(256, 512, 3), labelShape=(256, 512), \
		transforms=["none"], dataDimensions=["rgb"], labelDimensions=["depth"], \
		baseDataGroup="noseq", normalization="min_max_normalization")

	args = makeArgs({
		"dir" : "CityScapes-Norm-RGB-DNAE",
		"type" : sys.argv[1],
		"dataset" : "cityscapes"
	})

	corrupterReader = CorrupterReader(cityscapesReader, corruptionPercent=5, rangeValues=(0, 1))
	trainGenerator = corrupterReader.iterate("train", miniBatchSize=10, maxPrefetch=1)
	trainSteps = corrupterReader.getNumIterations("train", miniBatchSize=10)
	validationGenerator = corrupterReader.iterate("validation", miniBatchSize=10, maxPrefetch=1)
	validationSteps = corrupterReader.getNumIterations("validation", miniBatchSize=10)
	changeDirectory(args)

	callbacks = [SaveModelsSelfSupervised(), SaveModels("best")]
	model = maybeCuda(DNAEModel(inputShape=(256, 512, 3), numFilters=64))
	model.setOptimizer(optim.Adam, lr=0.001)
	model.setCriterion(lossFn)
	model.train_generator(trainGenerator, stepsPerEpoch=trainSteps, numEpochs=100, \
		callbacks=callbacks, validationGenerator=validationGenerator, validationSteps=validationSteps)

if __name__ == "__main__":
	main()
