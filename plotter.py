import sys
import numpy as np
from scipy.misc import toimage
from neural_wrappers.callbacks import Callback
from functools import partial
from neural_wrappers.readers.cityscapes_reader import cityscapesSemanticCmap
from collections import OrderedDict

# @brief Function that compute the number of channels and what each channel represents, based on an input
#  ArgumentParser object.
def getInputMetadata(args):
	numChannels = 3
	dimensionsIndexes = OrderedDict({"rgb" : (0, 3)})

	if args.inference_rgb_prev_frame:
		dimensionsIndexes["rgb_prev_frame"] = (numChannels, numChannels + 3)
		numChannels += 3

	# RGB First Frame
	if args.inference_rgb_older_frame:
		dimensionsIndexes["rgb_first_frame"] = (numChannels, numChannels + 3)
		numChannels += 3

	# Depth previous frame
	if args.inference_prev_depth:
		assert args.inference_prev_depth_mode in ("regular", "deeplabv3_new_dims", \
			"deeplabv3_foreground_background")
		# String is "depth_prev_frame_deeplabv3" or "depth_prev_frame_regular" in dataset.
		dimensionsIndexes["depth_prev_frame_" + args.inference_prev_depth_mode] = (numChannels, numChannels + 1)
		numChannels += 1

	# Optical flow algorithms (like FlowNet2S)
	if not args.inference_optical_flow is None:
		assert args.inference_optical_flow in ("flownet2s", )
		if args.inference_optical_flow_transform == "none":
			dimensionsIndexes[args.inference_optical_flow] = (numChannels, numChannels + 2)
			numChannels += 2
		elif args.inference_optical_flow_transform == "magnitude":
			dimensionsIndexes[args.inference_optical_flow] = (numChannels, numChannels + 1)
			numChannels += 1
		else:
			assert False, "Expected optical_flow_transform: \"none\" or \"magnitude\""

	# Semantic segmentation algorithms (like using ground truth or implementations like DeepLabV3)
	if not args.inference_semantic is None:
		assert args.inference_semantic in ("deeplabv3", "labels", "ground_truth_fine", "hvn")
		if args.inference_semantic_transform in ("default", "foreground-background", "none"):
			dimensionsIndexes[args.inference_semantic] = (numChannels, numChannels + 1)
			numChannels += 1
		elif args.inference_semantic_transform == "semantic_new_dims":
			# Different datasets have different number of classes
			if args.dataset == "cityscapes":
				N = 19
			elif args.dataset == "none":
				assert not args.inference_semantic_num_classes is None, "For inference mode, number of classes" + \
					" be guessed automatically, it must be specified."
				N = args.inference_semantic_num_classes
			else:
				N = args.nyudepth_semantic_num_classes
			dimensionsIndexes[args.inference_semantic] = (numChannels, numChannels + N)
			numChannels += N
		else:
			assert False, "Expected semantic_transform: \"foreground-background\", \"semantic_new_dims\", " + \
			"\"none\" or \"default\""

	if not args.inference_hvn is None:
		if args.inference_hvn_transform == "none":
			dimensionsIndexes["hvn"] = (numChannels, numChannels + 1)
			numChannels += 1
		elif args.inference_hvn_transform == "hvn_two_dims":
			dimensionsIndexes["hvn"] = (numChannels, numChannels + 2)
			numChannels += 2

	return numChannels, list(dimensionsIndexes.keys()), dimensionsIndexes

class PlotCallback(Callback):
	def __init__(self, plotResults, saveResults, paramsHandler):
		self.count = 1
		self.plotResults = plotResults
		self.saveResults = saveResults
		self.paramsHandler = paramsHandler

	def onIterationEnd(self, **kwargs):
		import matplotlib.pyplot as plt
		if not self.plotResults and not self.saveResults:
			return

		data = kwargs["data"]
		labels = kwargs["labels"]
		results = kwargs["results"]
		loss = kwargs["metrics"]["Loss"]

		for i in range(len(results)):
			images, titles, params = self.paramsHandler(data[i], labels[i], results[i])
			L2Diff = np.sum((labels[i] - results[i])**2)
			print("L2 Diff: %2.2f" % (L2Diff))
			nicePlotter(images=images, titles=titles, params=params)
			if self.saveResults:
				plt.savefig("%d.png" % (self.count), dpi=200)
				self.count += 1
				if self.count == 101:
					sys.exit(0)
			if self.plotResults:
				plt.gcf().set_size_inches(20, 8)
				plt.show(block=True)


def nicePlotter(images, titles, params):
	print(titles)
	import matplotlib.pyplot as plt
	numRows = len(images)
	plt.clf()
	plt.gcf().set_size_inches(15, 4 * numRows)

	numCols = 0
	for row in range(numRows):
		numCols = max(numCols, len(images[row]))

	for row in range(numRows):
		thisCols = len(images[row])
		rowStartIndex = numCols * row + 1
		for col in range(thisCols):
			image = images[row][col]
			if image.shape[-1] == 1:
				image = image[..., 0]
			image = np.array(toimage(image))
			title = titles[row][col]
			param = params[row][col]

			index = rowStartIndex + col
			plt.gcf().add_subplot(numRows, numCols, index)
			plt.imshow(image, **param)

			plt.gcf().gca().axis("off")
			if title:
				plt.gcf().gca().set_title(title)

def test_reader(generator, trainSteps, args):
	import matplotlib.pyplot as plt
	from Mihlib import npGetInfo
	from datetime import datetime
	print("Num steps for this generator: ", trainSteps)
	paramsHandlerCallback = partial(paramsHandler, args=args)

	now = datetime.now()
	for j, (data, depths) in enumerate(generator):
		print("Took %s" % (datetime.now() - now))
		for i in range(len(data)):
			print(npGetInfo(data[i]), "\n", npGetInfo(depths[i]))
			images, titles, params = paramsHandlerCallback(data[i], depths[i], result=None)
			nicePlotter(images=images, titles=titles, params=params)
			plt.show(block=True)
			plt.clf()
		now = datetime.now()

def addToCurrentRow(allData, data, titles, params={}, maxRowItems=4):
	if len(allData[0][-1]) == maxRowItems:
		allData = addNewRow(allData)
	row, rowTitles, rowParams = allData[0][-1], allData[1][-1], allData[2][-1]
	row.append(data)
	rowTitles.append(titles)
	rowParams.append(params)

def addNewRow(data):
	data[0].append([])
	data[1].append([])
	data[2].append([])
	return data

def paramsHandler(data, label, result, args):
	dims, _, dimensionsIndexes = getInputMetadata(args)
	allData = addNewRow([[], [], []])

	# First row is dedicated to RGB, Label, Result and Diff
	rgb = data[..., 0 : 3]
	from lycon import resize, Interpolation
	addToCurrentRow(allData, rgb, "RGB")

	if not label is None:
		addToCurrentRow(allData, label, "Label", {"cmap" : "hot"})

	if not result is None:

		addToCurrentRow(allData, result, "Result", {"cmap" : "hot"})

		if not label is None:
			diffImage = np.abs(label - result)
			diffL1 = np.sum(diffImage)
			addToCurrentRow(allData, diffImage, "Diff: %2.2f" % (diffL1), {"cmap" : "hot"})

	# 2nd (and potential 3rd row) is dedicated to the rest of the stuff used as data input
	#  rgb_first_frame, semantic, optical flow, previous depth etc..
	if args.inference_rgb_prev_frame or args.inference_rgb_older_frame or not args.inference_optical_flow is None or \
		not args.inference_semantic is None or args.inference_prev_depth:
		allData = addNewRow(allData)

	if args.inference_rgb_prev_frame:
		l, r = dimensionsIndexes["rgb_prev_frame"]
		addToCurrentRow(allData, data[..., l : r], "RGB previous frame")

	if args.inference_rgb_older_frame:
		l, r = dimensionsIndexes["rgb_first_frame"]
		addToCurrentRow(allData, data[..., l : r], "RGB older frame")

	if args.inference_prev_depth:
		assert args.inference_prev_depth_mode in ("regular", "deeplabv3_new_dims", \
			"deeplabv3_foreground_background")
		# String is "depth_prev_frame_deeplabv3" or "depth_prev_frame_regular" in dataset.
		l, r = dimensionsIndexes["depth_prev_frame_" + args.inference_prev_depth_mode]
		prevDepth = data[..., l : r]
		addToCurrentRow(allData, prevDepth[..., 0], "Depth prev frame (%s)" % (args.inference_prev_depth_mode), \
			{"cmap" : "hot"})

	if not args.inference_optical_flow is None:
		assert args.inference_optical_flow in ("flownet2s", )
		l, r = dimensionsIndexes[args.inference_optical_flow]
		flow = data[..., l : r]
		if args.inference_optical_flow_transform == "none":
			addToCurrentRow(allData, flow[..., 0], "Flow Y", {"cmap" : "hot"})
			addToCurrentRow(allData, flow[..., 1], "Flow X", {"cmap" : "hot"})
		elif args.inference_optical_flow_transform == "magnitude":
			addToCurrentRow(allData, flow[..., 0], "Flow Magnitude", {"cmap" : "hot"})

	if not args.inference_semantic is None:
		assert args.inference_semantic in ("deeplabv3", "labels", "ground_truth_fine")
		l, r = dimensionsIndexes[args.inference_semantic]
		semantic = data[..., l : r]
		if args.inference_semantic_transform == "semantic_new_dims":
			mask = np.ones(semantic.shape)

			# Different datasets, different num classes
			if args.dataset == "nyudepth":
				N = args.nyudepth_semantic_num_classes
			else:
				N = 19

			for i in range(N):
				mask[..., i] *= i
			semantic *= mask
			semantic = np.uint8(np.sum(semantic, axis=-1))
			addToCurrentRow(allData, semantic, "Semantic (%d dimensions)" % (N))
		elif args.inference_semantic_transform == "foreground-background":
			addToCurrentRow(allData, semantic[..., 0], "Semantic (foreground-background)", {"cmap" : "hot"})
		elif args.inference_semantic_transform == "default":
			semantic = cityscapesSemanticCmap(np.uint8(semantic[..., 0] * 33))
			addToCurrentRow(allData, semantic, "Semantic (default normalized)")
		elif args.inference_semantic_transform == "none":
			semantic = cityscapesSemanticCmap(np.uint8(semantic[..., 0]))
			addToCurrentRow(allData, semantic, "Semantic (no transforms)")

	if not args.inference_hvn is None:
		l, r = dimensionsIndexes["hvn"]
		hvn = data[..., l : r]
		if args.inference_hvn_transform == "none":
			addToCurrentRow(allData, np.uint8(hvn[..., 0]), "HVN (none)")
		elif args.inference_hvn_transform == "hvn_two_dims":
			mask = np.ones(hvn.shape[0 : -1], dtype=np.uint8) * 2
			whereH = np.where(hvn[..., 0] == 1)
			whereV = np.where(hvn[..., 1] == 1)
			mask[whereH] = 0
			mask[whereV] = 1
			addToCurrentRow(allData, mask, "HVN (two dims)")

	return allData