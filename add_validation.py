import os
import sys
import re
import torch as tr
from copy import deepcopy

# Nicely taken from StackOverflow natural sort
def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)

def flushPrint(x):
	sys.stdout.write("%s\n" % (x))
	sys.stdout.flush()

def loadUpdateSave(filePath, epoch, metrics):
	modelDict = tr.load(filePath)
	modelDict["history_dict"][epoch]["validationMetrics"] = deepcopy(metrics)
	tr.save(modelDict, filePath)
	del modelDict

def addValidation(model, datasetReader, modelsDir, batchSize, modelStart=None):
	assert not modelsDir is None

	os.chdir(modelsDir)
	weightFilesPaths = natural_sort(list(filter(lambda x : x.endswith(".pkl"), os.listdir())))

	if modelStart:
		lastModel = tr.load(modelStart)
		startEpoch = len(lastModel["history_dict"])
		flushPrint("Updating weights from epoch %d" % (startEpoch))
		for j in range(startEpoch, len(weightFilesPaths)):
			flushPrint("Model %d done" % (j + 1))

			modelDict = tr.load(weightFilesPaths[j])
			for i in range(startEpoch):
				modelDict["history_dict"][i]["validationMetrics"] = lastModel["history_dict"][i]["validationMetrics"]
			tr.save(modelDict, weightFilesPaths[j])
			del modelDict
	else:
		startEpoch = 0
		flushPrint("Updating weights from epoch 0")

	generator = datasetReader.iterate("validation", miniBatchSize=batchSize, maxPrefetch=1)
	numIterations = datasetReader.getNumIterations("validation", miniBatchSize=batchSize)

	for i in range(startEpoch, len(weightFilesPaths)):
		model.load_weights(weightFilesPaths[i])
		validationMetrics = model.test_generator(generator, numIterations)
		flushPrint("Epoch %d. Weights file: %s. Metrics: %s" % (i + 1, weightFilesPaths[i], validationMetrics))

		for j in range(i, len(weightFilesPaths)):
			flushPrint("Model %d done" % (j + 1))
			loadUpdateSave(weightFilesPaths[j], i, validationMetrics)
